" Curtis Sand's Vim/NeoVim Config
" This file should work with recent versions of Vim and NeoVim

" Config Section for the old Vim
if !has('nvim')
    " This line should not be removed as it ensures that various options are
    " properly set to work with the Vim-related packages available in Debian.
    runtime! debian.vim

    " Source a global configuration file if available
    if filereadable("/etc/vim/vimrc.local")
      source /etc/vim/vimrc.local
    endif

    " Enable mouse for resizing panes
    set mouse=n
    set ttymouse=xterm2

    set t_Co=256  " set terminal color mode for vim
else
    set mouse=a
endif

" Vim5 and later versions support syntax highlighting. Uncommenting the
" following enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype on
  filetype plugin indent on
endif
"
" I'm lazy
command! Q quit

" Reload vimrc when I modify it
autocmd BufWritePost .vimrc,init.vim,dot.vimrc source %

" Map <leader> to ","
let mapleader = ","

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd     	" Show (partial) command in status line.
set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden          " Hide buffers when they are abandoned

set number          " Enable Gutter with Line Numbers. Use 'set nonumber'
                    " or 'set nonu' to disable it.
map ,n :set invnumber<CR>

"forced backup behaviour
set nobackup    "no permanent backup file
set writebackup "write a backup during overwrite

set directory=${HOME}/.vim/cache/

" My Personal Settings
"" tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent

"" word wrapping
set textwidth=0
set wrapmargin=0
set linebreak
set formatoptions+=l

"" Highlight the 80th column
set colorcolumn=80
nnoremap <silent> <leader>c :execute "set colorcolumn=" . (&colorcolumn == "" ? "80" : "")<CR>

"" Folding
set foldmethod=indent
set foldnestmax=3       " don't nest more than 3 folds
set foldlevel=10        " start with all folds open

"" Highlighting
set hlsearch
map ,* :noh<CR>

"" resizing windows
map ,[ :vertical res -1<CR>
map ,] :vertical res +1<CR>
map ,9 :res -1<CR>
map ,0 :res +1<CR>
" more resizing options:
" <c-w>+ or <c-w>- for vertical resize
" <c-w>< or <c-w>> for horizontal resize
" N<c-w>_ set height to N
" N<c-w>| set width to N
" <c-w>= equalize size of windows

"" Navigating Between Split Windows with CTRL + Movement key
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Load markdown syntax highlighting for *.md, *.mkd, *.markdown
if has("autocmd")
    au BufNewfile,BufRead *.md set syntax=markdown
    au BufNewfile,BufRead *.mkd set syntax=markdown
    au BufNewFile,BufRead *.markdown set syntax=markdown
endif

" Load rest syntax highlighting for *.rst, *.txt
" also change some settings to make rest editing nicer
if has("autocmd")
    au BufNewfile,BufRead *.rst set syntax=rst |
        \ set foldmethod=marker |
        \ set foldlevel=0 |
        \ set nonumber |
        \ set textwidth=100 |
        \ set colorcolumn=101
    au BufNewfile,BufRead *.txt set syntax=rst |
        \ set foldmethod=marker |
        \ set foldlevel=0 |
        \ set nonumber |
        \ set textwidth=100 |
        \ set colorcolumn=101
endif

"" removes whitespace at end of line when saving
"autocmd BufWritePre *.py :%s/\s\+$//e  "python files only
"autocmd BufWritePre *.rst :%s/\s\+$//e "ReST files only
autocmd BufWritePre * :%s/\s\+$//e     "All files :D
autocmd BufWritePre * :%s/\($\n\s*\)\+\%$//e " remove trailing whitespace lines

" Use 2 space tabs in some filetypes
autocmd FileType html,htmldjango,javascript,scss,css setlocal tabstop=2 shiftwidth=2 softtabstop=2

"" Where to find tag files
set tags=./tags,./\.git/tags

" List Buffers
map ,4 :ls<CR>


"" Packages

" NERD Tree
map ,3 :NERDTreeToggle<CR>

" Fugitive configuration
map ,gs :Gstatus<CR>
map ,ga :Git add %<CR>
map ,gc :Gcommit<CR>
map ,gd :Gdiff HEAD<CR>
map ,gdc :Git diff --cached %<CR>
map ,gb :Gblame<CR>
map ,gpl :Git pull --rebase<CR>
map ,gpu :Git push<CR>

" ctrlp options
let g:ctrlp_working_path_mode='rw'
let g:ctrlp_follow_symlinks=1

" keybindings for scratch.vim
map ,s :Scratch<CR>
