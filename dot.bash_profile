export PKEY=~/.ssh/id_rsa.pub
export EDITOR='vim'
export ALTERNATEEDITOR='vim'
export TERM='xterm-256color'

alias reload='source ~/.bashrc'

#change keyboard layout
alias aoeu='setxkbmap us'
alias asdf='setxkbmap dvorak'

#silly common aliases
alias l='ls -lah --color=auto'       # Long view, show hidden
alias ls='ls -Fp --color=auto'      # Compact view, show colors
alias la='ls -AF --color=auto'       # Compact view, show hidden
alias ll='ls -lFh --color=auto'      # Long view, no hidden
alias lt='ls -t --color=auto'       # sort by timestamp
alias cp='cp -i' #prompt before overwrite
alias mv='mv -i' #prompt before overwrite
alias grep='grep --color=auto' # Always highlight grep search term
alias ping='ping -c 5'  # Pings with 5 packets, not unlimited
alias df='df -h'        # Disk free, in gigabytes, not bytes
alias du='du -h -c'     # Calculate total disk usage for a folder
alias ..='cd ..'        # Go up one directory
alias ...='cd ../..'    # Go up two directories
alias reboot='sudo shutdown -r now'
alias power='sudo shutdown -h now'
alias say='spd-say -r -30 -p 10 -t male2 '

#my useful aliases
alias free='free -m'
alias mytop='top -U csand'
alias size='du -s'
alias clear='clear; ls'
alias oo='openoffice.org3'
alias s='screen -DR'
alias play='DISPLAY=:0 vlc -I ncurses --extraintf=http --fullscreen'
alias rscp="nice rsync -haP --no-whole-file --inplace"
alias rsmv="nice rsync -haP --no-whole-file --inplace --remove-source-files"
alias vi="vim"
alias v="vim"
alias tc="sudo truecrypt -t --protect-hidden=no --keyfiles=''"
alias dt="~/stuff/scripts/detach"

# pandoc
alias md2rst="pandoc -f markdown -t rst "

#git
alias gad='git add'
alias gap='git apply'
alias gbr='git branch'
alias gch='git checkout'
alias gcl='git clone'
alias gco='git commit'
alias gdi='git diff'
alias gfe='git fetch'
alias ggr='git grep'
alias glo='git log'
alias gme='git merge'
alias gmv='git mv'
alias gpl='git pull'
alias gpu='git push'
alias grb='git rebase'
alias gre='git reset'
alias grm='git rm'
alias gsh='git show'
alias gst='git status'
alias gta='git tag'

#world ssh
alias com="ssh curtsan2@curtissand.com"
alias home='ssh -X csand@curtissand.is-a-geek.com'

#local ssh
alias alpha='ssh -X csand@192.168.0.100'
alias eeepc='ssh -X csand@192.168.0.111'

#Templates
alias newmake='cp -i ~/stuff/templates/makefile ./'
alias newc='cp -i ~/stuff/templates/cfile.c'
alias newh='cp -i ~/stuff/templates/hfile.h'
alias newreadme='cp -i ~/stuff/templates/readme ./'
alias newpy='cp -i ~/stuff/templates/python.py'
alias neworg='cp -i ~/stuff/templates/org.org'

#edit the path for good measure
PATH="${PATH}:/bin/"
PATH="${PATH}:/usr/bin"
PATH="${PATH}:/usr/local/bin"
PATH="${PATH}:/sbin"

PATH="${PATH}:/usr/X11R6/bin"
PATH="${PATH}:/usr/bin/X11"
PATH="${PATH}:/usr/games"

#Location Specific Section
if [[ `hostname` = "csand-beta" ]]; then
    PATH="/home/csand/stuff/scripts:${PATH}"
elif [[ `hostname` = "csand-alpha" ]]; then
    PATH="/home/csand/stuff/scripts:${PATH}"
fi


if [[ `hostname -y` = "spgear" ]]; then
    PATH="${PATH}:/msg/spgear/bin"
    PATH="${PATH}:/msg/spgear/tools/bin"
    PATH="${PATH}:$JAVA_HOME"
    PATH="${PATH}:/spgear/spgear/bin"
    PATH="${PATH}:/spgear/tools/bin"
    PATH="${PATH}:/spgear/zeph_comp_tools/jdk1.6.0_03/bin"
    PATH="${PATH}:/opt/openssh-2.9/bin"
    PATH="${PATH}:/opt/rational/clearcase/bin/"
    PATH="${PATH}:/home/runner/bin"
    if [[ `hostname` = "sandc3" ]]; then
        PATH="/local/sandc3/bin:/local/sandc3/stuff/scripts:${PATH}"
    fi
    PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.6/dist-packages/unittest2-0.5.1-py2.6.egg
    PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.6/dist-packages/mock-0.8.0alpha1-py2.6.egg
    PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.6/dist-packages/discover-0.4.0-py2.6.egg/
    PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.6/dist-packages/RBTools-0.3.2-py2.6.egg/
    PYTHONPATH=$PYTHONPATH:$PATH
    export PYTHONPATH
fi

export PATH
