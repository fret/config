#!/bin/bash

QB_SESSION="$(dirname $(readlink -f $BASH_SOURCE))/session.sh"

SESSION_NAME="$(zenity --entry --text 'start qutebrowser session')"

WINDOW_TITLE="{title_sep}{perc}{current_title}{title_sep}QB"

if [[ "${SESSION_NAME}" == "" ]] ; then
    exec ${QB_SESSION} --set window.title_format "default${WINDOW_TITLE}"
else
    exec ${QB_SESSION} -r ${SESSION_NAME} --set window.title_format "${SESSION_NAME}${WINDOW_TITLE}"
fi
