#!/bin/bash

# SOURCE, DEST and VAULT should end in "/"
SOURCE="/mnt/storage/"
DEST="/mnt/storage/archive/"
VAULT="/mnt/nas/backup/"

# SUFFIX should include it's own delimiter (i.e. ".")
SUFFIX=".tar"
GZ_SUFFIX=".tgz"

# DIRS is a space delimited list of directories to backup and should not
# contain the char "/"
DIRS="backgrounds pics"
DIRS_GZ="mail books"
tarballs=""

echo `date`

for dir in $DIRS; do
    tarballs="$tarballs $DEST$dir$SUFFIX";
    echo "Tarballing $dir..."
    nice tar cf $DEST$dir$SUFFIX $SOURCE$dir/ ;
done

for dir in $DIRS_GZ; do
    tarballs="$tarballs $DEST$dir$GZ_SUFFIX";
    echo "Tarballing $dir..."
    nice tar czf $DEST$dir$GZ_SUFFIX $SOURCE$dir/ ;
done

echo "Transferring tarballs to vault..."
echo "exec nice rsync -rhP --no-whole-file --inplace --delete $tarballs $VAULT"

echo "Archive and Vault Complete!"
