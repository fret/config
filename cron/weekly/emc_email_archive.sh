#!/bin/bash

RDIFF=/local/sandc3/code/rd-back.sh
OFFLINEIMAP=/local/sandc3/lib/offlineimap/offlineimap.py

EMAILS=/local/nobackup/emcmail
BACKUP=/local/nobackup/emcmail.bak
LOG=/local/sandc3/emailBackup.log

# Run Offlineimap then do a Live backup
$OFFLINEIMAP -o && $RDIFF -l $EMAILS $BACKUP && echo `date` >> $LOG
