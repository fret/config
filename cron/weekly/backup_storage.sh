#!/bin/bash

# SOURCE and DEST should end in "/"
SOURCE="/mnt/storage/"
DEST="/mnt/nas/"

# DIRS is a space delimited list of directories to backup and should not
# contain the char "/"
DIRS="music books pics"

for dir in $DIRS; do
    nice rsync -rhP --no-whole-file --inplace --delete $SOURCE$dir $DEST
done
