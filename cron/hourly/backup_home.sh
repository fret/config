#!/bin/bash

# SOURCE and DEST should end in "/"
SOURCE="/home/csand/"
DEST="/mnt/storage/backup/"

# DIRS is a space delimited list of directories to backup and should not
# contain the char "/"
DIRS="blog code config fret misc work"

for dir in $DIRS; do
    nice rsync -rhP --no-whole-file --inplace --delete $SOURCE$dir $DEST
done
