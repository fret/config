#!/bin/bash

# SOURCE, DEST and VAULT should end in "/"
SOURCE="/mnt/storage/backup/"
DEST="/mnt/storage/archive/"
VAULT="/mnt/nas/backup/"

# SUFFIX should include it's own delimiter (i.e. ".")
SUFFIX=".tgz"

# DIRS is a space delimited list of directories to backup and should not
# contain the char "/"
DIRS="blog code config fret misc work"
tarballs=""

echo `date`

for dir in $DIRS; do
    tarballs="$tarballs $DEST$dir$SUFFIX";
    echo "Tarballing $dir..."
    nice tar czf $DEST$dir$SUFFIX $SOURCE$dir/ ;
done

echo "Transferring tarballs to vault..."
exec nice rsync -rhP --no-whole-file --inplace --delete $tarballs $VAULT

echo "Archive and Vault Complete!"
