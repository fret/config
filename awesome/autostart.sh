#!/bin/bash

# Fret's Awesome WM Autostart Script

run_once () {
    local prog=${1}
    local args=${@:2:${#}}
    pidof ${prog} &>/dev/null || ${prog} ${args} &>/dev/null &
}

# enable logging of all stdout and stderr
LOG_FILE=/tmp/awesome_autostart.log
exec >> $LOG_FILE
exec 2>&1

echo "----"
echo "Awesome starting at $(date +%g%m%d-%H%M%S)"
start_time=$(date +%s)

## Set keyboard settings - 250 ms delay and 25 cps (characters per second) repeat rate.
## Adjust the values according to your preferances.
xset r rate 200 25 && echo "set Keyboard repeat rates" &

## Turn on/off system beep
xset b off && echo "turn bell off" &

## Turn on numlock
# note: on some systems it was "numlock" on fedora 21 it seems to be numlockx
numlockx on && echo "turn numlock on" &

## Enable power management
xfce4-power-manager && echo "xfce4 power manager" &

## Support for volume keys
xfce4-volumed && echo "xfce4 volumed" &

## Start Thunar Daemon
thunar --daemon && echo "thunar daemon" &

## Detect and configure touchpad. See 'man synclient' for more info.
if egrep -iq 'touchpad' /proc/bus/input/devices; then
    synclient VertEdgeScroll=1 &
    synclient TapButton1=1 &
    echo "touchpad config via synclient" &
fi

## Start xscreensaver
#xscreensaver -no-splash && echo "xscreensaver" &

## Start Clipboard manager
(sleep 3s && echo "clipit" && run_once clipit) &

# nm-applet
(sleep 1s && echo nm-applet && run_once "nm-applet &>/dev/null") &

## davmail
#(sleep 1s && run_once davmail) &

## pidgin
#(sleep 5s && NSS_SSL_CBC_RANDOM_IV=0 pidgin) &

## dropdown terminal
#(sleep 5 && echo guake &&run_once guake) &
(sleep 5 && echo tilda && run_once tilda) &

## Assume external monitor
#(sleep 1s && .screenlayout/two_external.sh) &
#/local/sandc3/.screenlayout/choose.sh &

elapsed_time=$((${start_time} - $(date +%s)))
echo "Awesome autostart completed in ${elapsed_time} seconds."
#zenity --info --text "autostart complete in ${elapsed_time} secs" 2>/dev/null

# dvorak
setxkbmap dvorak

# dropbox
(sleep 10 && echo dropbox && dropbox start) &

# steam
(sleep 10 && echo steam && run_once steam) &
