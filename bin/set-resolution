#!/bin/bash
# Set the screen resolution to any given resolution

# Note: Tested on a Fedora 31 Guest OS in a VirtualBox VM using the legacy
#       VBOXVGA driver.

DEBUG=0
SCRIPTNAME=$0

usage () {
    echo "$SCRIPTNAME [-h|--help] [-v|--verbose] WIDTH HEIGHT"
    echo "Set resolution to given width and height with xrandr"
    exit 1
}

dprint () {
    [[ $DEBUG -ne 0 ]] && echo $@
}

die () {
    echo $2 >&2
    exit $1
}

# use getopt to capture command line flags
ARGS=$(getopt -o "hv" -l "help,verbose" -n $SCRIPTNAME -- "$@")
[ $? -ne 0 ] && usage  # bad arguments found
eval set -- "$ARGS"
while true ; do
    case "$1" in
        -h|--help) usage ; shift ;;
        -v|--verbose) DEBUG=1 ; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

# capture width and height inputs from the command line
if [[ $# -ne 2 ]] ; then
    echo "Error: $0 requires both a width and a height input." >&2
    echo ""
    usage
fi
WIDTH=$1
HEIGHT=$2

# hardcoded for now, bad habit I know.
# TODO: create an option for this with a default value.
SCREEN="VGA-1"

EXISTING_MODE=$(xrandr 2>/dev/null | grep "^\s*${WIDTH}x${HEIGHT}" |\
                head -1 | sed -e 's/^\s*//' -e 's/ \+[0-9]*.*$//')
if [[ -n "$EXISTING_MODE" ]] ; then
    dprint "Using existing xrandr mode: $EXISTING_MODE"
    xrandr --output $SCREEN --mode $EXISTING_MODE
    exit $?
else
    dprint "No existing mode found for that geometry, adding one."
    MODELINE="$(cvt $WIDTH $HEIGHT |\
                sed -e 's/Modeline "//' -e '/^#/d' -e 's/_60\.00"//')"
    MODENAME="$(echo $MODELINE | sed -e 's/ \+.*$//')"
    xrandr --newmode $MODELINE
    rc=$?; [[ $rc -ne 0 ]] &&\
        die $rc "Error creating new xrandr mode: $MODELINE"
    xrandr --addmode $SCREEN $MODENAME
    rc=$?; [[ $rc -ne 0 ]] &&\
        die 1 "Error adding new xrandr mode to display $SCREEN: $MODENAME"
    xrandr --output $SCREEN --mode $MODENAME
    rc=$?; [[ $rc -ne 0 ]] && die 1 "Error setting new resolution: $MODENAME"
    dprint "Screen resolution set to new mode $MODENAME"
fi
