#!/bin/bash

if [[ "$(id -u)" != "0" ]]; then
    echo "Must be root to configure the joystick device.";
    exit 1;
fi

if [[ ! -e /dev/input/js0 ]]; then
    echo "No device found at /dev/input/js0, quitting.";
    exit 0;  # not a failure, just nothing to do
fi

jscal -s 6,1,0,508,508,1056800,1042436,1,0,509,509,1054724,1044464,1,0,128,128,4194176,4227201,1,0,128,128,4194176,4227201,1,0,0,0,536854528,536854528,1,0,0,0,536854528,536854528 /dev/input/js0
