#!/bin/bash

# This script initializes my tmux dev environment.
# session ${primary} is the main session
# session ${dropdown} is the guake session
# session '0' and '9' share window 1 for mutt

primary=0
dropdown=9
SHARED_MUTT_WINDOW=false # set to false to disable the shared window for Mutt

tmux new-session -d -s ${primary}
tmux new-session -d -s ${dropdown}

if $SHARED_MUTT_WINDOW; then
    # move window 1 in dropdown to window 2 - freeing up window 1
    tmux move-window -s ${dropdown}:1 -t ${dropdown}:2

    # link window 1 of primary into dropdown
    tmux link-window -s ${primary}:1 -t ${dropdown}:1

    # add new bash window to primary
    tmux new-window -t ${primary}:2

    # open mutt in the shared window
    sleep 1
    tmux send-keys -t ${primary}:1 "mutt" "c-m"
fi
