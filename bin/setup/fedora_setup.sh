#!/bin/bash

# Frets Setup Script: configure a linux workstation lickity split

VERSION="fedora_21_20150104"

# TODO List:
# - Add undo methods for each step and provide user with undo mechanism
# - let user change username and/or path to home directory
# - let user choose between bashrc files (i.e. work vs home)

main () {
    parse_opts ${@}
    if ${DRY_RUN}; then
        echo "DRY_RUN is ON"
    fi

    check_root_user

    print_info
    enter_to_continue

    cd_home

    # software section
    if ${SKIP_SOFTWARE}; then
        echo -e "\nSkipping Software installation/upgrade...\n"
    else
        yum_upgrade
        add_rpm_fusion_repos
        install_fedy
        install_software
        run_fedy
        yum_upgrade
    fi

    # config section
    if ${SKIP_CONFIG}; then
        echo -e "\nSkipping Config phase...\n"
    else
        generate_ssh_key
        clone_repositories
        setup_bashrc
        link_config_files
    fi

    # clean up section
    if ${SKIP_CLEANUP}; then
        echo -e "\nSkipping Clean Up phase...\n"
    else
        clean_up
        set_home_ownership
    fi

    restore_cwd

    echo -e "\nSetup Complete"
}

print_info () {
    echo "Frets Setup Script (version=${VERSION})"
    echo
    echo "Configure a linux workstation lickity-split.  The following steps will be done:"
    echo "  - use user home directory: ${HOME_DIR}"
    echo "  - set up both the free and non-free rpmFusion repositories"
    echo "  - install the Fedy tool for fedora"
    echo "  - install some new software: ${SOFTWARE}"
    echo "  - run the fedy tweak tool for fedora"
    echo "  - upgrade the software packages through yum"
    echo "  - generate a new SSH key"
    echo "  - clone Frets repositories: ${REPOSITORIES}"
    echo "  - set up Frets bashrc"
    echo "  - link to various config files"
    echo "  - clean up unused targets in the home dir"
    echo
}

usage () {
    print_info
    echo "Options:"
    echo "  -h | --help     : Print this help text."
    echo "  -d | --dry_run  : Perform a dry run, make no changes to the system."
    echo "  -y | --yes      : Assume yes for all prompts."
    echo "  --https         : Use Https when cloning the git repositories."
    echo "  --skip-software : Skip the phase that upgrades/installs software."
    echo "  --skip-config   : Skip the phase that configures the users home dir."
    echo "  --skip-cleanup  : Skip the clean up phase."
    echo
}

parse_opts () {
    # Options with no args have no colon
    # Option with required arg has 1 colon
    # Option with optional arg has 2 colons
    ARGS=$(getopt --options "dhy" \
                 --long "dry-run,help,yes,https,skip-software,skip-config,skip-cleanup" \
                 --name "$SCRIPT" -- "$@")

    if [ $? -ne 0 ]; then
        # bad arguments found
        usage;
        exit 1;
    fi
    eval set -- "$ARGS"

    local do_usage=false
    while true ; do
        case "$1" in
            -h|--help) do_usage=true; shift ;;
            -d|--dry-run) DRY_RUN=true; shift ;;
            -y|--yes) FORCE_YES=true; shift ;;
            --https) CLONE_FN=clone_repo_https; shift ;;
            --skip-software) SKIP_SOFTWARE=true; shift ;;
            --skip-config) SKIP_CONFIG=true; shift ;;
            --skip-cleanup) SKIP_CLEANUP=true; shift ;;
            --) shift ; break ;;
            *) echo "not recognized $1" ; exit 1 ;;
        esac
    done

    if ${do_usage}; then
        usage;
        exit 0;
    fi
}

enter_to_continue () {
    if ${FORCE_YES}; then
        return
    else
        echo "press enter to continue..."
        read
    fi
}

die () {
    echo "${2}"
    exit ${1}
}

do_conditionally () {
    local action=${1}
    local command=${2}
    if ${FORCE_YES}; then
        ${command}
        return
    fi
    local yes="y Y yes Yes YES YEs yEs yES yeS"
    local no="n N no No NO nO"
    local input
    local outer_break=false
    while :; do
        echo
        echo -e ${action}
        echo Enter yes to continue...
        read input
        for test in $yes; do
            if [[ ${input} == ${test} ]]; then
                ${command}
                outer_break=true
                break
            fi
        done
        for test in $no; do
            if [[ ${input} == ${test} ]]; then
                echo "Skipping step."
                outer_break=true
                break
            fi
        done
        [ $outer_break ] && break
    done
}

user_edit_software () {
    local msg="Edit the list of software to install"
    SOFTWARE=$(zenity ${ZENITY_OPTS} --entry --text "${msg}" \
               --entry-text "${SOFTWARE}" 2>/dev/null)
}

user_edit_repos () {
    local msg="Edit the list of repos to clone"
    REPOSITORIES=$(zenity ${ZENITY_OPTS} --entry --text "${msg}" \
                   --entry-text "${REPOSITORIES}" 2>/dev/null)
}

user_edit_clean_up_targets () {
    local msg="Edit the list of clean up targets"
    CLEAN_UP_TARGETS=$(zenity ${ZENITY_OPTS} --entry --text "${msg}" \
                       --entry-text "${CLEAN_UP_TARGETS}" 2>/dev/null)
}

check_root_user () {
    local msg="Please run script as root..."
    if [[ $(id -u) != 0 ]]; then
        if ${DRY_RUN}; then
            echo "${msg}"
            echo -e "   Ignoring user, for dry-run...\n"
        else
            die 1 "${msg}"
        fi
    fi
}

cd_home () {
    pushd ${HOME_DIR} &>/dev/null
}

restore_cwd () {
    popd &>/dev/null
}

add_rpm_fusion_repos () {
    local msg="Adding rpmFusion Repositories..."
    local cmd="yum localinstall -y --nogpgcheck \
        http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
        http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
    if ${DRY_RUN}; then
        cmd="echo ${cmd}"
    fi
    do_conditionally "${msg}" "${cmd}"
}

yum_upgrade () {
    local msg="Upgrading system..."
    local cmd="yum upgrade -y"
    if ${DRY_RUN}; then
        cmd="echo ${cmd}"
    fi
    do_conditionally "${msg}" "${cmd}"
}

install_software () {
    echo "\nInstalling some new software."
    do_conditionally "Edit the list of software?" user_edit_software
    local cmd="yum install -y ${SOFTWARE}"
    if ${DRY_RUN}; then
        cmd="echo ${cmd}"
    fi
    do_conditionally "Install the list of software?" "${cmd}"
}

clone_repo_git_proto () {
    su csand -c "git clone git@bitbucket.org:fret/${1}.git;"
}

clone_repo_https () {
    su csand -c "git clone https://fret@bitbucket.org/fret/${1}.git"
}

_clone_repositories () {
    local msg="Edit the repository list before cloning?"
    do_conditionally "${msg}" user_edit_repos
    for repo in ${REPOSITORIES}; do
        if [ -d ${repo} ] ; then
            echo "skipping clone of ${repo}, already exists"
        else
            if ${DRY_RUN}; then
                echo "dry_run: cloning ${repo} using ${CLONE_FN}"
            else
                ${CLONE_FN} ${repo}
            fi
        fi
    done
}

clone_repositories () {
    local msg="Cloning Frets repositories: ${REPOSITORIES}"
    do_conditionally "${msg}" _clone_repositories
}

_setup_bashrc () {
    local move="su csand -c \"mv .bashrc .bashrc_$(date +%g%m%d-%H%M)\""
    local link="su csand -c \"ln -s /home/csand/config/dot.bashrc_personal .bashrc\""
    if [[ -f .bashrc ]] ; then
        if ${DRY_RUN}; then
            echo "${move}"
            echo "${link}"
        else
            ${move}
            ${link}
        fi
    fi
}

setup_bashrc () {
    local msg="Setting up Frets bashrc"
    do_conditionally "${msg}" _setup_bashrc
}

_link_config_files () {
    if ${DRY_RUN}; then
        echo "ln -s config/dot.vimrc .vimrc"
        echo "ln -s config/dot.vim .vim"
        echo "ln -s config/dot.gitconfig .gitconfig"
        echo "ln -s config/dot.tmux.conf .tmux.conf"
        echo "mkdir -p .config/awesome/"
        echo "ln -s config/awesome/rc.lua .config/awesome/rc.lua"
    else
        su csand -c "ln -s /home/csand/config/dot.vimrc .vimrc"
        su csand -c "ln -s /home/csand/config/dot.vim .vim"
        su csand -c "ln -s /home/csand/config/dot.gitconfig .gitconfig"
        su csand -c "ln -s /home/csand/config/dot.tmux.conf .tmux.conf"
        su csand -c "mkdir -p .config/awesome/"
        su csand -c "ln -s /home/csand/config/awesome/rc.lua .config/awesome/rc.lua"
        su csand -c "ln -s /home/csand/config/awesome/autostart.sh config/awesome/autostart.sh"
    fi
}

link_config_files () {
    local msg="Linking to various config files"
    do_conditionally "${msg}" _link_config_files
}

_install_fedy () {
    if ${DRY_RUN}; then
        echo "curl https://satya164.github.io/fedy/fedy-installer -o fedy-installer"
        echo "chmod +x fedy-installer"
        echo "./fedy-installer"
    else
        curl https://satya164.github.io/fedy/fedy-installer -o fedy-installer
        chmod +x fedy-installer
        ./fedy-installer
    fi
}

install_fedy () {
    local msg="Installing Fedy tool"
    do_conditionally "${msg}" _install_fedy
}

run_fedy () {
    do_conditionally "Running fedy..." fedy
}

clean_up () {
    local msg="Edit the targets to clean up?"
    do_conditionally "${msg}" user_edit_clean_up_targets
    msg="Clean up, removing: ${CLEAN_UP_TARGETS}"
    do_conditionally "${msg}" "rm -rf ${CLEAN_UP_TARGETS}"
}

set_home_ownership () {
    local cmd="chown -R csand:csand *"
    echo "Setting home ownership with: ${cmd}"
    ${cmd}
}

_generate_ssh_key () {
    local dir_name="/home/csand/.ssh"
    local prv_name="${dir_name}/id_rsa"
    local pub_name="${dir_name}/id_rsa.pub"
    if ${DRY_RUN}; then
        echo "Dry Run: SSH Key generated..."
        return
    fi
    if [ ! -d ${dir_name} ]; then
        su csand -c "mkdir ${dir_name}"
    fi
    if [ -f ${prv_name} ] || [ -f ${pub_name} ]; then
    local msg="Remove existing key before proceeding?"
        do_conditionally "${msg}" "rm ${prv_name} ${pub_name}"
    fi
    su csand -c "mkdir -p ${dir_name} && \
        ssh-keygen -f ${prv_name} -N \"\" && \
        zenity ${ZENITY_OPTS} --text-info --filename ${pub_name} 2>/dev/null"
}

generate_ssh_key () {
    local msg="Generate an SSH Key?"
    do_conditionally "${msg}" _generate_ssh_key
}

# Global Variables
DRY_RUN=false
FORCE_YES=false
HOME_DIR="/home/csand"
SOFTWARE="vim git tmux awesome unetbootin htop vlc python-pip guake arandr"
REPOSITORIES="code fret curtissand space config music"
CLONE_FN=clone_repo_git_proto
CLEAN_UP_TARGETS="Documents Downloads fedy_dl fedy-installer Music Pictures Public Templates Videos"
ZENITY_OPTS="--width 800 --height 300"
SKIP_SOFTWARE=false
SKIP_CONFIG=false
SKIP_CLEANUP=false

# Execute the script
main ${@}
