#!/bin/bash

# rsync script to backup my pc with my nas

# note: excluded in this script are large dirs which are separated into their
#       own script

# note: the delete flag is used on these dirs so that removal is propagated

rsync="rsync -haP --no-group --no-perms --no-times --no-xattrs --delete --stats --size-only"

local_storage="/mnt/storage2Tb"
nas_storage="/mnt/nas/storage"

dirnames="books calibre misc projects tmp lib hpe"

log_dir="/mnt/storage2Ta/logs/nas"
mkdir ${log_dir} &>/dev/null

for dname in ${dirnames}; do
    ${rsync} ${local_storage}/${dname}/ ${nas_storage}/${dname}/ |\
        tee ${log_dir}/${dname}-$(date +%y%m%d%H%M).log
done
