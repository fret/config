#!/bin/bash

# rsync script to backup my pc with my nas

# note: the --delete flag is NOT used because not all of the files in the NAS
#       archive necessarily exist in the PC side of the storage.

rsync="rsync -haP --no-group --no-perms --no-times --no-xattrs --stats --size-only"

local_storage="/mnt/storage2Tb"
nas_storage="/mnt/nas/storage"

dirnames="backgrounds pics vids audiobooks"

log_dir="/mnt/storage2Ta/logs/nas"
mkdir ${log_dir} &>/dev/null

for dname in ${dirnames}; do
    ${rsync} ${local_storage}/${dname}/ ${nas_storage}/${dname}/ |\
        tee ${log_dir}/${dname}-$(date +%y%m%d%H%M).log
done
