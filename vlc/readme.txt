VLC Playlist Extension for Youtube Playlists
============================================

Put the "playlist_youtube.luac" file into
"${HOME}/.local/share/vlc/lua/playlist/" and ensure it is executable. Then
restart VLC or reload the extensions to activate it.

Original source from https://github.com/vvasuki/vlc-addons
