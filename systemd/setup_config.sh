#!/bin/bash

if [[ "$(id -u)" != "0" ]]; then
    echo "Must be run with root authority.";
    exit 1;
fi

if [[ ! -d ${1} ]]; then
    echo "First argument must be a directory of systemctl user units.";
    exit 1;
fi

echo "Disabling all units already configured..."
sudo systemctl disable $(ls ${1})

echo -e "\nCopying in new set of units from '${1}'..."
for f in $(ls ${1}); do
    echo "    $f"
    cp -f ${1}/$f /etc/systemd/system/$f;
done
echo ""

echo "Enabling new custom units..."
sudo systemctl enable $(ls ${1})
