#!/bin/bash

if [[ "$(id -u)" != "0" ]]; then
    echo "Must be run with root authority.";
    exit 1;
fi

if [[ ! -d ${1} ]]; then
    echo "First argument must be a directory of systemctl user units.";
    exit 1;
fi

for f in $(ls ${1}); do
    echo -e "$f;"
    diff ${1}/$f /etc/systemd/system/$f;
    echo -e "/$f\n";
done
