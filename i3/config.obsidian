# Curtis Sand's i3 Window Manager Configuration
#
# i3 config file (v4) : written for version
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!


## general configuration options

set $mod Mod4
font pango:DejaVu Sans Mono 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# window borders
default_border normal 1
hide_edge_borders both
title_align center

# show window marks
show_marks yes


## keybindings

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +3% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -3% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# start a terminal
# bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+Return exec kitty --single-instance --instance-group=i3 --listen-on=unix:/tmp/kittysocket

# kill focused window
bindsym $mod+Shift+quotedbl kill

# start dmenu (a program launcher)
bindsym $mod+e exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
bindsym $mod+j exec --no-startup-id i3-dmenu-desktop

# change focus
bindsym $mod+h focus left
bindsym $mod+t focus down
bindsym $mod+n focus up
bindsym $mod+s focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+H move left
bindsym $mod+Shift+T move down
bindsym $mod+Shift+N move up
bindsym $mod+Shift+S move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# move workspaces between monitors
bindsym $mod+Control+h move workspace to output left
bindsym $mod+Control+Left move workspace to output left
bindsym $mod+Control+t move workspace to output down
bindsym $mod+Control+Down move workspace to output down
bindsym $mod+Control+n move workspace to output up
bindsym $mod+Control+Up move workspace to output up
bindsym $mod+Control+s move workspace to output right
bindsym $mod+Control+Right move workspace to output right

# split in horizontal orientation
bindsym $mod+d split h

# split in vertical orientation
bindsym $mod+k split v

# enter fullscreen mode for the focused container
bindsym $mod+u fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+o layout stacking
bindsym $mod+comma layout tabbed
bindsym $mod+period layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+c focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+J reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+P restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+greater exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# scratchpad
# make currently focused window a scratchpad
bindsym $mod+Shift+minus move scratchpad
# show the first scratchpad window
bindsym $mod+minus scratchpad show

# toggle bar mode and state
bindsym $mod+Shift+b bar hidden_state toggle
bindsym $mod+b bar mode toggle

# toggle title bar using border
bindsym $mod+x border toggle

# marking windows
bindsym $mod+m exec i3-input -F 'mark %s' -l 1 -P 'Mark: '
bindsym $mod+g exec i3-input -F '[con_mark="%s"] focus' -l 1 -P 'Goto: '
bindsym $mod+Shift+m exec i3-input -F 'unmark %s' -l 1 -P 'Unmark: '

# switch between keyboard layouts
bindsym $mod+grave exec /home/csand/git/config/bin/kbdtoggle

# switch between screen layouts
bindsym $mod+F1 exec /home/csand/git/config/bin/sr


## i3 modes

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.

    # coarse precision
    bindsym h resize shrink width 10 px or 10 ppt
    bindsym t resize grow height 10 px or 10 ppt
    bindsym n resize shrink height 10 px or 10 ppt
    bindsym s resize grow width 10 px or 10 ppt

    # medium precision
    bindsym Shift+h resize shrink width 5 px or 5 ppt
    bindsym Shift+t resize grow height 5 px or 5 ppt
    bindsym Shift+n resize shrink height 5 px or 5 ppt
    bindsym Shift+s resize grow width 5 px or 5 ppt

    # fine precision
    bindsym Control+h resize shrink width 2 px or 2 ppt
    bindsym Control+t resize grow height 2 px or 2 ppt
    bindsym Control+n resize shrink height 2 px or 2 ppt
    bindsym Control+s resize grow width 2 px or 2 ppt

    # same bindings, but for the arrow keys
    # coarse precision
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # medium precision
    bindsym Shift+Left resize shrink width 5 px or 5 ppt
    bindsym Shift+Down resize grow height 5 px or 5 ppt
    bindsym Shift+Up resize shrink height 5 px or 5 ppt
    bindsym Shift+Right resize grow width 5 px or 5 ppt

    # fine precision
    bindsym Control+Left resize shrink width 2 px or 2 ppt
    bindsym Control+Down resize grow height 2 px or 2 ppt
    bindsym Control+Up resize shrink height 2 px or 2 ppt
    bindsym Control+Right resize grow width 2 px or 2 ppt

    # back to normal: Enter or Escape or $mod+r
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+p mode "default"
}

bindsym $mod+p mode "resize"

# launcher mode
mode "launcher" {
    bindsym v exec pavucontrol; mode "default"
    bindsym f exec firefox --new-window; mode "default"
    bindsym c exec kcolorchooser; mode "default"
    bindsym i exec inkscape; mode "default"
    bindsym s exec steam; mode "default"
    bindsym g exec gwenview ~; mode "default"
    bindsym q exec /home/csand/git/config/qutebrowser/start_qutebrowser.sh; mode "default"
    bindsym m exec /home/csand/storage/lib/minecraft-launcher/minecraft-launcher; mode "default"
    bindsym t exec spotify; mode "default"
    bindsym b exec blueman-manager; mode "default"
    bindsym y exec vlc https://www.youtube.com/playlist?list=PLc6PuSJrdVgst4iLtf9Utt7-SXh6a_C6C; mode "default"
    bindsym Shift+y exec vlc https://www.youtube.com/playlist?list=PLc6PuSJrdVgvEYqYry6nb0MirJGH6xUUi; mode "default"

    set $help "Launcher bindings:\n\nv:pavucontrol\nf:firefox\nc:kcolorchooser\ni:inkscape\ns:steam\nq:qutebrowser\nm:minecraft\nb:blueman\ny:youtube\n"
    bindsym slash exec zenity --info --text $help; mode "default"
    bindsym Shift+slash exec zenity --info --text $help; mode "default"

    # back to normal mode: Enter, Escape or $mod-;
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+semicolon mode "default"
}

bindsym $mod+semicolon mode "launcher"

# clicker mode
mode "clicker" {
    bindsym d exec xdotool mousedown 1; mode "default"
    bindsym i exec xdotool mouseup 1; mode "default"

    # back to normal mode: Enter, Escape or $mod-;
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+semicolon mode "default"
}

bindsym $mod+q mode "clicker"


## bar

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
  status_command i3status
	position top
	tray_output primary
	mode dock
	hidden_state hide
	modifier Mod4
}

## startup applications

# Xfce4 Panel
# exec --no-startup-id killall xfce4-panel; xfce4-panel --disable-wm-check

# Polybar Panel
# exec_always --no-startup-id killall polybar; polybar -r main

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xset s 14400 14400
exec --no-startup-id xset dpms 14400 14400 14400
# exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock -efc 002b36 --nofork

# Start Variety wallpaper manager
exec --no-startup-id variety

# Start guake dropdown terminal
#exec --no-startup-id guake

# Start dropbox app
exec --no-startup-id dropbox start


## workspaces

# Workspaces Setup. Odds on left screen, Evens on right screen.
# workspace 1 output HDMI-0
# workspace 3 output HDMI-0
# workspace 5 output HDMI-0
# workspace 7 output HDMI-0
# workspace 9 output HDMI-0
# workspace 2 output DVI-I-1
# workspace 4 output DVI-I-1
# workspace 6 output DVI-I-1
# workspace 8 output DVI-I-1
# workspace 10 output DVI-I-1


## Window Specific Settings

# pulesaudio volume controller
#for_window [class="Pavucontrol"] move container to workspace 10, mark --replace v
for_window [class="Pavucontrol"] mark --replace v, floating enable

# blueman-manager bluetooth manager gui
#for_window [class="Blueman-manager"] move container to workspace 10, mark --replace b
for_window [class="Blueman-manager"] mark --replace b, floating enable

# ktimer
for_window [class="ktimer"] mark --replace t, floating disable

# force title bar for qute browser
#   for some reason it is always disabled
for_window [class="qutebrowser"] border normal 0

# dunst notifications
for_window [class="Dunst"] floating enable
