(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(calendar-date-display-form (quote (year "-" month "-" day)))
 '(calendar-time-display-form (quote (24-hours "" minutes)))
 '(column-number-mode t)
 '(diary-file "~/.personalLog")
 '(european-calendar-style t)
 '(fringe-mode nil nil (fringe))
 '(gnus-ignored-newsgroups "")
 '(inhibit-startup-screen t)
 '(mark-diary-entries-in-calendar t t)
 '(org-link-frame-setup (quote ((vm . vm-visit-folder) (gnus . gnus) (file . find-file))))
 '(org-tab-follows-link t)
 '(python-python-command "ipython -cl")
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(visual-line-fringe-indicators (quote (left-curly-arrow nil))))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

(defun require-or-nil (feature)
  "If `feature' exists, require it, else return `nil'."
  (if (locate-library (format "%s" feature))
      (require feature)
    nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Behaviour

;; Put autosave files (ie #foo#) in one place, *not*
;; scattered all over the file system!
(defvar autosave-dir "~/.emacs_autobak/")
(make-directory autosave-dir t)
(defun auto-save-file-name-p (filename)
  (string-match "^#.*#$" (file-name-nondirectory filename)))
(defun make-auto-save-file-name ()
  (concat autosave-dir
   (if buffer-file-name
      (concat "#" (file-name-nondirectory buffer-file-name) "#")
    (expand-file-name
     (concat "#%" (buffer-name) "#")))))

;; Put backup files (ie foo~) in one place too. (The backup-directory-alist
;; list contains regexp=>directory mappings; filenames matching a regexp are
;; backed up in the corresponding directory. Emacs will mkdir it if necessary.)
(defvar backup-dir "~/.emacs_bak/")
(setq backup-directory-alist (list (cons "." backup-dir)))

; only require 'y' or 'n' for Yes/No questions
(defalias 'yes-or-no-p 'y-or-n-p)

; navigate through tag definitions
(global-set-key (kbd "M-<up>") 'beginning-of-defun)
(global-set-key (kbd "M-<down>") 'end-of-defun)

;; set auto-fill-mode to on by default
(setq auto-fill-mode 1)

; make 'M-x term' use 'M-x ansi-term' instead
(defalias 'term 'ansi-term)

; enable narrowing
(put 'narrow-to-region 'disabled nil)

; Enable the mouse wheel
(mouse-wheel-mode 1)

; Make the mouse work in terminals
(xterm-mouse-mode 1)

; If this isn't nil, there's a chance one of those terrible gtk file
; dialogs may show up.
(set-variable 'use-file-dialog nil)

;; Always syntax highlight
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)

;; tab-key always inserts spaces
(setq-default indent-tabs-mode nil)

;; Global indentation rules
(setq-default indent-tabs-mode nil
              standard-indent 4)

;; Ensure newline at EOF
(setq require-final-newline t)
(setq next-line-add-newlines nil)
(set-variable 'next-line-extends-end-of-buffer nil)

;;diary stuff
(setq view-diary-entries-initially nil)
(setq mark-diary-entries-in-calendar t)
(setq mark-holidays-in-calendar t)
; not sure i want this
;(add-hook 'diary-display-hook 'fancy-diary-display)
(add-hook 'list-diary-entries-hook 'sort-diary-entries t)
(add-hook 'list-diary-entries-hook 'include-other-diary-files)
(add-hook 'mark-diary-entries-hook 'mark-included-diary-files)
(add-hook 'today-visible-calendar-hook 'calendar-star-date)

;;; w3m
(set-variable 'w3m-use-cookies t)
(set-variable 'w3m-key-binding 'info)
(set-variable 'w3m-default-display-inline-images t)
(set-variable 'mm-w3m-safe-url-regexp nil)

;fix keybindings for outline modes
(when (require-or-nil 'outline)
  (defvar outline-short-prefix-map
    (let ((map (make-sparse-keymap)))
      (define-key map "@" 'outline-mark-subtree)
      (define-key map "n" 'outline-next-visible-heading)
      (define-key map "p" 'outline-previous-visible-heading)
      (define-key map "i" 'show-children)
      (define-key map "s" 'show-subtree)
      (define-key map "d" 'hide-subtree)
      (define-key map "u" 'outline-up-heading)
      (define-key map "f" 'outline-forward-same-level)
      (define-key map "b" 'outline-backward-same-level)
      (define-key map "t" 'hide-body)
      (define-key map "a" 'show-all)
      (define-key map "c" 'hide-entry)
      (define-key map "e" 'show-entry)
      (define-key map "l" 'hide-leaves)
      (define-key map "k" 'show-branches)
      (define-key map "q" 'hide-sublevels)
      (define-key map "o" 'hide-other)
      (define-key map "^" 'outline-move-subtree-up)
      (define-key map "v" 'outline-move-subtree-down)
      (define-key map "<" 'outline-promote)
      (define-key map ">" 'outline-demote)
      (define-key map "m" 'outline-insert-heading)
      map))
  (define-key outline-minor-mode-map (kbd "C-c o") outline-short-prefix-map)
)

;add outline minor mode for code folding
(add-hook 'python-mode-hook 'my-python-hook)
(defun my-python-hook ()
  (outline-minor-mode t)
)

;add outline minor mode for code folding
(add-hook 'markdown-mode-hook 'my-markdown-hook)
(defun my-markdown-hook()
  (outline-minor-mode t)
)

(add-to-list 'auto-mode-alist '("\\.md\\'"  .  markdown-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Display

; Remove startup message
(setq inhibit-startup-message t)

; Hide menu bar, tool bar, and scroll bar
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode -1)

; set color theme
(require 'color-theme)
;(color-theme-initialize)
;(color-theme-gnome2) 
;(color-theme-goldenrod)
(color-theme-calm-forest)

; set font
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-9"))

; create a buffer for the buffer menu
(global-set-key (kbd "C-x C-b") 'buffer-menu)

; make paren matches visible
(show-paren-mode 1)

; Disable blinking cursor
(blink-cursor-mode 0)

;; Display line and column numbers
(setq line-number-mode t)
(setq column-number-mode t)

;; Display the time and date
(setq display-time-day-and-date t)
(set-variable 'display-time-load-average nil)
(set-variable 'display-time-24hr-format t)
(display-time)

;; Highlight TODO
(let ((todo-modes '(c-mode c++-mode csharp-mode java-mode asm-mode
                    common-lisp-mode emacs-lisp-mode lisp-mode haskell-mode
                    perl-mode php-mode python-mode ruby-mode
                    apache-mode nxml-mode css-mode
                    latex-mode tex-mode asy-mode)))
  (dolist (mode todo-modes)
    (font-lock-add-keywords
     mode
     '(("\\<\\(TODO\\):" 1 font-lock-warning-face t)))))

(autoload 'which-function "which-func")
(defun which-func ()
  "Mention the current function name in the echo area."
  (interactive)
  (message (concat "Current function: " (which-function))))

;; No zoning!!!
;(zone-when-idle 0)

;; Act as a server unless a server is already running. Old emacs
;; doesn't have server-running-p, but we don't like their server
;; anyway.
(require 'server)
(when (and (fboundp 'server-running-p)
           (not (server-running-p)))
  (server-start))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  DEPRECATED

;; ;add my python customization
;; (add-hook 'python-mode-hook 'my-python-hook)
;; ;this gets called by outline to deteremine the level. Just use the
;; ;length of the whitespace 
;; (defun py-outline-level ()
;;   (let (buffer-invisibility-spec)
;;     (save-excursion
;;       (skip-chars-forward "\t ")
;;       (current-column))))
;; ; this get called after python mode is enabled
;; (defun my-python-hook ()
;;   ; outline uses this regexp to find headers. I match lines with no
;;   ; indent and indented "class" and "def" lines.
;;   (setq outline-regexp "[^ \t]\\|[ \t]*\\(def\\|class\\) ")
;;   ; enable our level computation
;;   (setq outline-level 'py-outline-level)
;;   ; do not use their \C-c@ prefix, too hard to type. Note this
;;   ; overides some python mode bindings 
;;   (setq outline-minor-mode-prefix "\C-c")
;;   ; turn on outline mode
;;   (outline-minor-mode t)
;;   ; initially hide all but the headers
;;   ;; (hide-body)
;;   ; I use CUA mode on the PC so I rebind these to make the more accessible
;;   ;(local-set-key [?\C-\t] 'py-shift-region-right)
;;   ;(local-set-key [?\C-\S-\t] 'py-shift-region-left) 
;; )
