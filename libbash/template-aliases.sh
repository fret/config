#location of the templates
TEMPLATE_REPO=${HOME}/git/code/templates/

#Template aliases
for template in $(find ${TEMPLATE_REPO} -type f); do
  name=$(echo $(echo $template|sed 's/.*\///g')| cut -d '.' -f '1');
  alias new${name}=cp\ -i\ ${template};
done
