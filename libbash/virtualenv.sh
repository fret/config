#!/bin/bash

# Parent dir for the virtualenvs
# ensure VIRTUALENV_DIR ends with a "/"
# different value for different host
case `hostname -s` in
    obsidian) VIRTUALENV_DIR=/home/csand/virt_envs/ ;;
    hackmanite) VIRTUALENV_DIR=/home/csand/virt_envs/ ;;
    *) VIRTUALENV_DIR=~/virt_envs/ ;;
esac
# make global
export VIRTUALENV_DIR

# Activate a particular virtualenv.
# If no virtualenv specified then print available
act () {
    local file=$VIRTUALENV_DIR$1/bin/activate;
    if [[ $# -eq 1 && -f $file ]]; then
        source $file;
    else
        ls $VIRTUALENV_DIR;
    fi;
};

# shorten deactivate command
alias deact="deactivate"
