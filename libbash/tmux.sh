function _tmux_get_unique_id() {
    local _id=$(tmux list-sessions|grep "^$1"|wc -l)
    local _unique=0
    while [[ $_unique != 1 ]]; do
        _id=$(($_id + 1))
        local clientid=$1-$_id
        tmux list-sessions | grep -q "^$clientid"
        _unique=$?
    done
    echo $clientid
}

function tma() {
    local clientid=$(_tmux_get_unique_id $1)
    if [[ $# -eq 2 ]]; then
        tmux new-session -d -t $1 -s $clientid \; \
            set destroy-unattached on \; attach-session -t $clientid \; \
            select-window -t $2 \; refresh-client -t $clientid \;
    else
        tmux new-session -d -t $1 -s $clientid \; \
            set destroy-unattached on \; attach-session -t $clientid \; \
            refresh-client -t $clientid \;
    fi
}

function tm() {
    (tmux list-sessions|grep "^${1}" &>/dev/null) || \
        tmux new-session -d -s $1
    tma $1 $2
}

function tmc() {
    local name=""
    if [[ $# -eq 2 ]]; then
        name="-n ${2}"
    fi
    local clientid=$(_tmux_get_unique_id $1)
    tmux new-session -d -t $1 -s $clientid \; \
        set destroy-unattached on \; new-window $name \; \
        attach-session -t $clientid \; \
        refresh-client -t $clientid \;
}

alias tml="tmux list-sessions"
alias tmk="tmux kill-session -t"

# Get/Set TMUX Environment variables
alias tset="tmux set-env -g "

function tshow () {
    if [[ "$1" != "" ]]; then
        tmux show-environment -g $1 | sed 's/^.*=//'
    else
        tmux show-environment -g
    fi
}
