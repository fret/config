alias superhot="/mnt/btrfs-storage/SteamLibrary/steamapps/common/SUPERHOT/SUPERHOT.x86_64"

alias minecraft="/home/csand/storage/lib/minecraft-launcher/minecraft-launcher"

# Endless Sky
alias endless-sky="steam steam://rungameid/404410"

# Joystick Testing
alias jstest="sudo jstest /dev/input/js0"
