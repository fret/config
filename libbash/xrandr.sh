# libbash: xrandr aliases for changing screen size of a local VM

# This whole thing depends on where `<repo>/config/libbash` is compared to
# `<repo>/tools/xrandr/set-resolution`
# If necessary update SET_RES to point to your "set-resolution" script.

_REPO="$(dirname $(dirname $(readlink -f $BASH_SOURCE)))";
SET_RES="${_REPO}/bin/set-resolution";
_REPO=;

alias set-res="$SET_RES"
alias sr-small="$SET_RES 800 600"
alias sr-tall="$SET_RES 800 950"
alias sr-lg="$SET_RES 1104 950"
alias sr-wide="$SET_RES 1600 950"
alias sr-nfull="$SET_RES 1890 1000"
alias sr-full="$SET_RES 1920 1080"
alias sr-nqhd="$SET_RES 2544 1400"
alias sr-qhd="$SET_RES 2560 1440"
