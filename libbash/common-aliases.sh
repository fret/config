# Determine where the bash profile lives
BASH_PROFILE=""
_BASHRC=${HOME}/.bashrc
_PROFILE=${HOME}/.profile
if [ -f ${_BASHRC} ]; then
    BASH_PROFILE=${_BASHRC}
elif [ -f ${_PROFILE} ]; then
    BASH_PROFILE=${_PROFILE}
fi

# reload bashrc
alias reload='source ${BASH_PROFILE}'

# change keyboard layout
alias aoeu='setxkbmap -layout us -option ctrl:swapcaps'
alias asdf='setxkbmap -layout dvorak -option ctrl:swapcaps'
alias kbd='${HOME}/config/bin/keyboard'

# xrandr setups
alias xrandr.laptop="/home/csand/.screenlayout/laptop-only.sh"
alias xrandr.hdmi="/home/csand/.screenlayout/hdmi-1280-768.sh"

# Determine if this version of "ls" supports the "--color" option.
LS_COLOR=
USE_LS_COLOR="$(ls --version &>/dev/null; echo $?)"
if [ $USE_LS_COLOR -eq 0 ]; then
    LS_COLOR="--color=auto"
fi

# silly common aliases
alias l='ls -lah  ${LS_COLOR}'       # Long view, show hidden
alias ls='ls -Fp  ${LS_COLOR}'       # Compact view, show colors
alias la='ls -AF  ${LS_COLOR}'       # Compact view, show hidden
alias ll='ls -lFh ${LS_COLOR}'       # Long view, no hidden
alias lt='ls -t   ${LS_COLOR}'       # sort by timestamp
alias cp='cp -i' #prompt before overwrite
alias mv='mv -i' #prompt before overwrite
alias grep='grep --color=auto' # Always highlight grep search term
alias ping='ping -c 5'  # Pings with 5 packets, not unlimited
alias df='df -h'        # Disk free, in gigabytes, not bytes
alias du='du -h -c'     # Calculate total disk usage for a folder
alias ..='cd ..'        # Go up one directory
alias ...='cd ../..'    # Go up two directories
alias acpi='acpi -abt'
alias apt='sudo aptitude'
alias less='less -R'
alias d='dirs'

# my useful aliases
alias free='free -m'
alias mytop='top -U csand'
alias size='du -s'
alias clear='clear; ls'
alias rscp="nice rsync -haP --no-whole-file --inplace"
alias rsmv="nice rsync -haP --no-whole-file --inplace --remove-source-files"
alias dt="detach"
alias vol="alsamixer"
alias web="w3m"
alias ddg="w3m duckduckgo.com"
alias lsdisks='ls /dev/sd*'
alias pg="ps aux | grep"

# VLC
alias play='DISPLAY=:0 vlc -I ncurses --extraintf=http --fullscreen --one-instance'
alias queue='vlc --one-instance --playlist-enqueue'

# GPG
# only use gpg2
alias gpg="gpg2"
alias enc="gpg2 --encrypt --sign --symmetric --armor"
alias share="gpg2 --encrypt --sign --armor"

# I hate typos
alias gerp="grep"

# usefull unison aliases
alias sync="unison -ui text -batch -logfile /tmp/unison-$(date +%y%m%d%H%m).log"
alias syncm="unison -ui text -logfile /tmp/unison-$(date +%y%m%d%H%m).log"

# Fret's Dir aliases: set aliases for quick movement with pushd
#
# Note: disabling this loop for now because I'm collecting a lot more
#       repositories than I expected when this was added.
#
# for dir in $(find -L ${HOME}/git -maxdepth 1 -type d -not -iname "git"); do
#   name=$(echo $dir|sed 's/.*\///g');
#   alias ${name}=pushd\ ${HOME}/git/${name};
# done;

alias config="pushd ${HOME}/config"
alias fret="pushd ${HOME}/git/fret"
alias code="pushd ${HOME}/git/code"
alias stg="pushd ${HOME}/storage"
alias lib="pushd ${HOME}/lib"

# GPG
alias gpg='gpg2'
alias enc='gpg2 --encrypt --sign --armor --recipient "Curtis Sand"'

# pidgin : admins at work can't security
alias pidgin='NSS_SSL_CBC_RANDOM=0 NSS_ALLOW_WEAK_SIGNATURE=1 /usr/bin/pidgin '

# Systemd aliases
alias userctl="systemctl --user"

alias sc="sudo systemctl"
alias scs="sudo systemctl status"
alias sclu="sudo systemctl list-units"
alias scluf="sudo systemctl list-unit-files"

alias sc-deps='sudo systemctl list-dependencies'
alias sc-mine='sudo systemctl status $(sudo find /etc/systemd/system -maxdepth 1 -type f | cut -d / -f 5) --full |less'
alias sc-services='sudo systemctl -t service'
alias sc-targets='sudo systemctl -t target'

alias offlineimap_log="journalctl -ef -u offlineimap"

# docker aliases
alias dkr='sudo docker'

# Create tags files for python projects
alias ptags="ctags --totals --recurse --python-types --fields=+f --tag-relative=yes -o tags -L *"

# python unittest discover
alias discover="python -m unittest discover"

# ImageMagick aliases
alias mogrify_orient="mogrify -auto-orient"
alias mogrify_thumbs="mogrify -auto-orient -format png -thumbnail '250x250>' -path thumbs"
alias img_size="identify -format '%[fx:w]x%[fx:h]' "

# Use cat as a scratchpad
alias scratch="cat - <<EOF >/dev/null"

# ipython
alias ipy="ipython3"

# run my custom suspend script
alias suspend="sudo systemctl suspend"

# list the existing qutebrowser sessions by date
alias qbs="ls -tl1cr ${HOME}/.var/state/qutebrowser/*/state | sed -e 's@^.*csand csand [0-9]* \(.*\) /home.*qutebrowser/\(.*\)/state@\1   \2@'"

# DNF
alias dnfi="sudo dnf install"
alias dnfu="sudo dnf upgrade"
alias dnfs="sudo dnf search"
alias dnfgl="sudo dnf group list"
alias dnfgi="sudo dnf group info"

# light aliases
alias bl="/usr/bin/light"
alias kbdl="light -s sysfs/leds/asus::kbd_backlight"

# Kitty theme changer
alias kt="kitty-theme"
