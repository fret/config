# Editor Settings

# Try to use neovim if it's available
_vim='vim'
if [[ 1 -eq $(which nvim 2>/dev/null | wc -l) ]]; then
    _vim='nvim'
    export NVIM_LISTEN_ADDRESS=/tmp/nvimsocket
fi

EDITOR=$_vim
export EDITOR
export ALTERNATEEDITOR='vi'

# Keep editor aliases close to EDITOR environment variable
alias vi=$_vim
alias v=$_vim
alias vimnw="$_vim -c \"set eventignore=BufWritePre\" " # don't clobber trailing whitespace
alias view="$_vim -R "

# export TERM='xterm-256color'

# setup the default PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
export PATH=${PATH}:${HOME}/bin/:${HOME}/git/config/bin:${HOME}/.local/bin


NOOUT="&>/dev/null"

title () {
    echo -ne "\e]0;$1\a"
}

import () {
    if [[ -z $LIBBASH ]]; then
        echo "Please define LIBBASH in ~/.bashrc"
    else
        source ${LIBBASH}${1}
    fi
}

# Usage: mid <from> <to> <file>
# print lines <from> to <to> from <file>
mid () {
    tail -n +${1} ${3} | head -n ${2}
}

# unify popd and pushd into pd
pd () {
    if [[ $# -eq 0 ]]; then
        popd
    else
        pushd $1
    fi
}

# shortcut for 'watch -n 1 "tail -n XX file"' where XX is set dynamically
follow () {
    temp=$(stty size | cut -d " " -f 1)
    termsize=$(($temp - 10))
    if [[ -z $1 ]]; then
        echo "follow expects a file argument."
    else
        echo "Viewing last $termsize lines..."
        watch -n 1 "tail -n $termsize $1"
    fi
}

# find the newest file in a directory
newest () {
    if [[ ! -d $1 ]]; then
        ls -1tr . | tail -n 1
    else
        if [[ ${1:$((${#1}-1)):1} != "/" ]]; then
            dir="$1/"
        else
            dir=$1
        fi
        echo $dir$(ls -1tr $dir | tail -n 1)
    fi
}

# Print a line of characters a given width
pwidth () {
    local width=80
    if [[ ! -z $1 ]]; then
        width=$1
    fi
    python -c "print('*' * $width)"
}

term_widths () {
    for wd in 80 100 120 ; do
        echo $wd wide;
        pwidth $wd;
    done
}

# Print a column of characters a given number of lines high
pheight () {
    local height=40
    if [[ ! -z $1 ]]; then
        height=$1
    fi
    local index=0
    while [[ $index -lt $height ]]; do
        echo '*';
        index=$(( $index + 1 ))
    done
}

# A quick countdown function, waits a given number of seconds while printing a
# message every so often
COUNTDOWN_STEP=5
function countdown () {
    local count=$1
    local indent="    "
    while [ $count -gt 0 ]; do
        echo -ne "${indent}${count}${indent}\r"
        if [ $count -le $(($COUNTDOWN_STEP * 2)) ]; then
            sleep 1
            count=$(($count - 1))
        else
            sleep $COUNTDOWN_STEP
            count=$(($count - $COUNTDOWN_STEP))
        fi
    done
    if [[ -z $2 ]]; then
        echo -e "${indent}${count}${indent}${indent}"
    else
        echo -e "${indent}${2}${indent}${indent}"
    fi
}

blog () {
    test -z $1 && echo "need title of the blog entry..." && return 1
    pushd ~/git/fret &>/dev/null;
    local dir="blog/$(date +%Y-%m)"
    if [[ ! -d $dir ]]; then
        echo "  making month dir"
        mkdir $dir
    fi
    local fname="${dir}/$(~/git/code/str-sanitize ${1}).rst";
    local tmpl_len="$(($(wc -l template.rst|sed 's@^ *@@g'|cut -d " " -f 1) - 1))";
    echo "$1" > $fname;
    tail -n $tmpl_len template.rst >> $fname;
    vim $fname;
    popd &>/dev/null;
}

calc () {
    echo "${@}" | bc
}

pydiscover () {
  python -m unittest discover $1 --verbose 2>&1 | sed '/^test.*ok/d'
}

# functions for getting date strings for timestamps and stuff
DATE_ISO_YEAR_PATTERN="%y"

second () {
    date +${DATE_ISO_YEAR_PATTERN}%m%d%H%M%S
}

minute () {
    date +${DATE_ISO_YEAR_PATTERN}%m%d%H%M
}

hour () {
    date +${DATE_ISO_YEAR_PATTERN}%m%d%H
}

day () {
    date +${DATE_ISO_YEAR_PATTERN}%m%d
}

month () {
    date +${DATE_ISO_YEAR_PATTERN}%m%d
}

year () {
    date +${DATE_ISO_YEAR_PATTERN}
}


# XDG_DATA_DIRS setting for flatpak
XDG_DATA_DIRS=${XDG_DATA_DIRS}:/var/lib/flatpak/exports/share
XDG_DATA_DIRS=${XDG_DATA_DIRS}:${HOME}/.local/share/flatpak/exports/share

# Add bash completion for kitty terminal if installed
which kitty &>/dev/null && source <(kitty + complete setup bash)

# Prevent Python 3 from writing a __pycache__ folder
export PYTHONDONTWRITEBYTECODE=1

# simplify common lastpass (lpass) actions
alias lpl="lpass ls"
alias lpp="lpass show --password --clip"
LASTPASS_COLOR=always
lp () {
    lpass show --color=$LASTPASS_COLOR $1 | grep -vi -e "password" -e "passwd" -e "pwd"
}

# neovim and neovim-remote
vcd() {
    nvr -c "cd $1"
}
