WORD_DIFF="" #"--word-diff=plain"

# List all of the aliases in this bash git library
alias git_aliases="grep '^alias' ~/git/config/libbash/git.sh | sed -e 's|alias ||g' -e 's|=|\t|'"

# Aliases to speed up git usage
alias gad='git add'
alias gap='git apply'
alias gbr='git branch'
alias gch='git checkout'
alias gcl='git clone'
alias gco='git commit'
alias gcp='git cherry-pick'
alias gdi="git diff --color --patience $WORD_DIFF"
alias gfe='git fetch'
alias ggr='git grep'
alias glo='git log'
alias glc='git log --cherry'
alias gme='git merge'
alias gmv='git mv'
alias gpl='git fetch && git pull'
alias gpu='git push'
alias grb='git rebase'
alias grc='git rebase --continue'
alias gra='git rebase --abort'
alias gre='git reset'
alias grm='git rm'
alias gsh='git show --color --patience'
alias gst='git status -sb'
alias gta='git tag'
alias gls='git ls-files'

alias gstash='git stash'
alias gdic="git diff --color --patience --cached $WORD_DIFF"
alias gcoa='git commit --amend'
alias gplr='git pull --rebase'
alias gplrpu='git pull --rebase && git push'
alias gstat='git diff --numstat'
alias gfpl='git fetch && gpl --rebase'
alias gclf='git clean -fd'
alias gloo='git log --pretty=format:"%h %<(12,trunc)%ai%x08%x08 %an:%x09%s" -$(($(stty size|cut -f 1 -d " ")-2))'
alias glom='git log --pretty=format:"%h %<(12,trunc)%ai%x08%x08 %an:%x09%s" -$(($(stty size|cut -f 1 -d " ")-2))|grep $(date +"%Y-%m")'

alias ghide='git update-index --assume-unchanged '
alias glhid='git ls-files -v | grep -e "^[hsmrck]"'
alias gunhide='git update-index --no-assume-unchanged '

gitf () {
    # Use '--short' instead of porcelain because it gives relative paths from
    # the CWD.
    git status --short | sed -e 's/^...//g'
}

# Some personal environment config
# note: don't include trailing slash, use ${HOME} instead of "~"
export FRETS_REPOS=$(find -L ${HOME}/git -maxdepth 1 -type d -not -iname "git")

# helper functions
_getBranchName () {
    # CWD must be in repo
    # $1 is the work-tree of the repository
    branch_name=$(git symbolic-ref -q HEAD)
    branch_name=${branch_name##refs/heads/}
    branch_name=${branch_name:-HEAD}
    echo $branch_name
}

_isDirty () {
    # CWD must be in repo
    git diff-index --quiet HEAD
    echo $?
}

# print status of my repos
st () {
    for repo in $FRETS_REPOS ; do
        local name=$(echo $repo|sed 's|\(.*/\)*||')
        [ -d $repo/.git ] || continue
        echo "$name status:"
        git -C $repo status -sb ;
        echo ""
    done;
}

# pull each of my repos if they're clean
pl () {
    for repo in $FRETS_REPOS ; do
        [ -d $repo/.git ] || continue
        local name=$(echo $repo|sed 's|\(.*/\)*||')
        echo "Updating $name..."
        pushd $repo &> /dev/null
        local branch_name=$(_getBranchName)
        local local_changes=$(_isDirty)
        if [[ $branch_name == "master" && $local_changes -eq 0 ]] ; then
            # update repo if branch == master and no local changes
            git pull --rebase origin master ;
        else
            echo "Skipping $name because:"
            git status -sb ;
        fi
        popd &> /dev/null
        echo ""
    done;
}

lo () {
    for repo in $FRETS_REPOS ; do
        [ -d $repo/.git ] || continue
        local name=$(echo $repo|sed 's|\(.*/\)*||')
        echo "$name status:"
        git -C $repo log --pretty=format:"%h%x09%s" -10;
        echo ""
    done;
}

git_ssl_verify () {
    echo "Toggling the GIT_SSL_NO_VERIFY environment variable."
    if [[ -z $GIT_SSL_NO_VERIFY ]]; then
        echo "  unset, setting true"
        export GIT_SSL_NO_VERIFY=true
    elif [[ ! $GIT_SSL_NO_VERIFY ]]; then
        echo "  set to false, toggling"
        export GIT_SSL_NO_VERIFY=true
    else
        echo "  re-enabling SSL checking"
        unset GIT_SSL_NO_VERIFY
    fi
    echo "  new value: GIT_SSL_NO_VERIFY=$GIT_SSL_NO_VERIFY"
}
