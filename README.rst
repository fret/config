Fret's Config Repository
========================

This is a collection of my configuration files from 2012 onward.

Many files are old and not used anymore. Some files that are used are old and
have crufty stuff in them.

Whatever the state, this is my config. There are many others like it, but this
one is mine.
